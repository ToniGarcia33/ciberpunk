﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form4
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Cerrar = New System.Windows.Forms.Button()
        Me.VolverInicio = New System.Windows.Forms.Button()
        Me.Persona12 = New System.Windows.Forms.Button()
        Me.Persona11 = New System.Windows.Forms.Button()
        Me.Persona10 = New System.Windows.Forms.Button()
        Me.Persona9 = New System.Windows.Forms.Button()
        Me.Persona8 = New System.Windows.Forms.Button()
        Me.Persona7 = New System.Windows.Forms.Button()
        Me.Persona6 = New System.Windows.Forms.Button()
        Me.Persona5 = New System.Windows.Forms.Button()
        Me.Persona4 = New System.Windows.Forms.Button()
        Me.Persona3 = New System.Windows.Forms.Button()
        Me.Persona2 = New System.Windows.Forms.Button()
        Me.Persona1 = New System.Windows.Forms.Button()
        Me.Logo = New System.Windows.Forms.PictureBox()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Cerrar
        '
        Me.Cerrar.Location = New System.Drawing.Point(371, 245)
        Me.Cerrar.Margin = New System.Windows.Forms.Padding(2)
        Me.Cerrar.Name = "Cerrar"
        Me.Cerrar.Size = New System.Drawing.Size(76, 29)
        Me.Cerrar.TabIndex = 51
        Me.Cerrar.Text = "Cerrar"
        Me.Cerrar.UseVisualStyleBackColor = True
        '
        'VolverInicio
        '
        Me.VolverInicio.Location = New System.Drawing.Point(371, 212)
        Me.VolverInicio.Margin = New System.Windows.Forms.Padding(2)
        Me.VolverInicio.Name = "VolverInicio"
        Me.VolverInicio.Size = New System.Drawing.Size(76, 29)
        Me.VolverInicio.TabIndex = 53
        Me.VolverInicio.Text = "Volver"
        Me.VolverInicio.UseVisualStyleBackColor = True
        '
        'Persona12
        '
        Me.Persona12.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona12
        Me.Persona12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona12.Location = New System.Drawing.Point(284, 194)
        Me.Persona12.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona12.Name = "Persona12"
        Me.Persona12.Size = New System.Drawing.Size(80, 80)
        Me.Persona12.TabIndex = 11
        Me.Persona12.UseVisualStyleBackColor = True
        '
        'Persona11
        '
        Me.Persona11.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona11
        Me.Persona11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona11.Location = New System.Drawing.Point(194, 194)
        Me.Persona11.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona11.Name = "Persona11"
        Me.Persona11.Size = New System.Drawing.Size(80, 80)
        Me.Persona11.TabIndex = 10
        Me.Persona11.UseVisualStyleBackColor = True
        '
        'Persona10
        '
        Me.Persona10.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona10
        Me.Persona10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona10.Location = New System.Drawing.Point(104, 194)
        Me.Persona10.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona10.Name = "Persona10"
        Me.Persona10.Size = New System.Drawing.Size(80, 80)
        Me.Persona10.TabIndex = 9
        Me.Persona10.UseVisualStyleBackColor = True
        '
        'Persona9
        '
        Me.Persona9.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona9
        Me.Persona9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona9.Location = New System.Drawing.Point(14, 194)
        Me.Persona9.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona9.Name = "Persona9"
        Me.Persona9.Size = New System.Drawing.Size(80, 80)
        Me.Persona9.TabIndex = 8
        Me.Persona9.UseVisualStyleBackColor = True
        '
        'Persona8
        '
        Me.Persona8.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona9
        Me.Persona8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona8.Location = New System.Drawing.Point(284, 104)
        Me.Persona8.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona8.Name = "Persona8"
        Me.Persona8.Size = New System.Drawing.Size(80, 80)
        Me.Persona8.TabIndex = 7
        Me.Persona8.UseVisualStyleBackColor = True
        '
        'Persona7
        '
        Me.Persona7.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona7
        Me.Persona7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona7.Location = New System.Drawing.Point(194, 104)
        Me.Persona7.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona7.Name = "Persona7"
        Me.Persona7.Size = New System.Drawing.Size(80, 80)
        Me.Persona7.TabIndex = 6
        Me.Persona7.UseVisualStyleBackColor = True
        '
        'Persona6
        '
        Me.Persona6.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona6
        Me.Persona6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona6.Location = New System.Drawing.Point(104, 104)
        Me.Persona6.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona6.Name = "Persona6"
        Me.Persona6.Size = New System.Drawing.Size(80, 80)
        Me.Persona6.TabIndex = 5
        Me.Persona6.UseVisualStyleBackColor = True
        '
        'Persona5
        '
        Me.Persona5.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona5
        Me.Persona5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona5.Location = New System.Drawing.Point(14, 104)
        Me.Persona5.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona5.Name = "Persona5"
        Me.Persona5.Size = New System.Drawing.Size(80, 80)
        Me.Persona5.TabIndex = 4
        Me.Persona5.UseVisualStyleBackColor = True
        '
        'Persona4
        '
        Me.Persona4.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona4
        Me.Persona4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona4.Location = New System.Drawing.Point(284, 14)
        Me.Persona4.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona4.Name = "Persona4"
        Me.Persona4.Size = New System.Drawing.Size(80, 80)
        Me.Persona4.TabIndex = 3
        Me.Persona4.UseVisualStyleBackColor = True
        '
        'Persona3
        '
        Me.Persona3.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona3
        Me.Persona3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona3.Location = New System.Drawing.Point(194, 14)
        Me.Persona3.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona3.Name = "Persona3"
        Me.Persona3.Size = New System.Drawing.Size(80, 80)
        Me.Persona3.TabIndex = 2
        Me.Persona3.UseVisualStyleBackColor = True
        '
        'Persona2
        '
        Me.Persona2.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona2
        Me.Persona2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona2.Location = New System.Drawing.Point(104, 14)
        Me.Persona2.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona2.Name = "Persona2"
        Me.Persona2.Size = New System.Drawing.Size(80, 80)
        Me.Persona2.TabIndex = 1
        Me.Persona2.UseVisualStyleBackColor = True
        '
        'Persona1
        '
        Me.Persona1.BackgroundImage = Global.Civerpunck.My.Resources.Resources.persona1
        Me.Persona1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Persona1.Location = New System.Drawing.Point(14, 14)
        Me.Persona1.Margin = New System.Windows.Forms.Padding(5)
        Me.Persona1.Name = "Persona1"
        Me.Persona1.Size = New System.Drawing.Size(80, 80)
        Me.Persona1.TabIndex = 0
        Me.Persona1.UseVisualStyleBackColor = True
        '
        'Logo
        '
        Me.Logo.Image = Global.Civerpunck.My.Resources.Resources.logo
        Me.Logo.Location = New System.Drawing.Point(388, 12)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(80, 80)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Logo.TabIndex = 54
        Me.Logo.TabStop = False
        '
        'Form4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(480, 288)
        Me.Controls.Add(Me.Logo)
        Me.Controls.Add(Me.VolverInicio)
        Me.Controls.Add(Me.Cerrar)
        Me.Controls.Add(Me.Persona12)
        Me.Controls.Add(Me.Persona11)
        Me.Controls.Add(Me.Persona10)
        Me.Controls.Add(Me.Persona9)
        Me.Controls.Add(Me.Persona8)
        Me.Controls.Add(Me.Persona7)
        Me.Controls.Add(Me.Persona6)
        Me.Controls.Add(Me.Persona5)
        Me.Controls.Add(Me.Persona4)
        Me.Controls.Add(Me.Persona3)
        Me.Controls.Add(Me.Persona2)
        Me.Controls.Add(Me.Persona1)
        Me.Name = "Form4"
        Me.Text = "Form4"
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Cerrar As Button
    Friend WithEvents VolverInicio As Button
    Friend WithEvents Persona12 As Button
    Friend WithEvents Persona11 As Button
    Friend WithEvents Persona10 As Button
    Friend WithEvents Persona9 As Button
    Friend WithEvents Persona5 As Button
    Friend WithEvents Persona6 As Button
    Friend WithEvents Persona7 As Button
    Friend WithEvents Persona8 As Button
    Friend WithEvents Persona4 As Button
    Friend WithEvents Persona3 As Button
    Friend WithEvents Persona2 As Button
    Friend WithEvents Persona1 As Button
    Friend WithEvents Logo As PictureBox
End Class
