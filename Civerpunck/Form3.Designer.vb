﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.IdViviendaLabel = New System.Windows.Forms.Label()
        Me.SectorLabel = New System.Windows.Forms.Label()
        Me.EdificioLabel = New System.Windows.Forms.Label()
        Me.PlantaLabel = New System.Windows.Forms.Label()
        Me.PuertaLabel = New System.Windows.Forms.Label()
        Me.SectorDato = New System.Windows.Forms.TextBox()
        Me.EdificioDato = New System.Windows.Forms.TextBox()
        Me.PlantaDato = New System.Windows.Forms.TextBox()
        Me.PuertaDato = New System.Windows.Forms.TextBox()
        Me.Cerrar = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Volver = New System.Windows.Forms.Button()
        Me.IdViviendaDato = New System.Windows.Forms.TextBox()
        Me.Logo = New System.Windows.Forms.PictureBox()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'IdViviendaLabel
        '
        Me.IdViviendaLabel.AutoSize = True
        Me.IdViviendaLabel.Location = New System.Drawing.Point(26, 27)
        Me.IdViviendaLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.IdViviendaLabel.Name = "IdViviendaLabel"
        Me.IdViviendaLabel.Size = New System.Drawing.Size(62, 13)
        Me.IdViviendaLabel.TabIndex = 4
        Me.IdViviendaLabel.Text = "ID Vivienda"
        '
        'SectorLabel
        '
        Me.SectorLabel.AutoSize = True
        Me.SectorLabel.Location = New System.Drawing.Point(26, 81)
        Me.SectorLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.SectorLabel.Name = "SectorLabel"
        Me.SectorLabel.Size = New System.Drawing.Size(38, 13)
        Me.SectorLabel.TabIndex = 5
        Me.SectorLabel.Text = "Sector"
        '
        'EdificioLabel
        '
        Me.EdificioLabel.AutoSize = True
        Me.EdificioLabel.Location = New System.Drawing.Point(26, 119)
        Me.EdificioLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.EdificioLabel.Name = "EdificioLabel"
        Me.EdificioLabel.Size = New System.Drawing.Size(41, 13)
        Me.EdificioLabel.TabIndex = 6
        Me.EdificioLabel.Text = "Edificio"
        '
        'PlantaLabel
        '
        Me.PlantaLabel.AutoSize = True
        Me.PlantaLabel.Location = New System.Drawing.Point(26, 159)
        Me.PlantaLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.PlantaLabel.Name = "PlantaLabel"
        Me.PlantaLabel.Size = New System.Drawing.Size(37, 13)
        Me.PlantaLabel.TabIndex = 7
        Me.PlantaLabel.Text = "Planta"
        '
        'PuertaLabel
        '
        Me.PuertaLabel.AutoSize = True
        Me.PuertaLabel.Location = New System.Drawing.Point(26, 197)
        Me.PuertaLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.PuertaLabel.Name = "PuertaLabel"
        Me.PuertaLabel.Size = New System.Drawing.Size(38, 13)
        Me.PuertaLabel.TabIndex = 8
        Me.PuertaLabel.Text = "Puerta"
        '
        'SectorDato
        '
        Me.SectorDato.Location = New System.Drawing.Point(92, 78)
        Me.SectorDato.Margin = New System.Windows.Forms.Padding(2)
        Me.SectorDato.Name = "SectorDato"
        Me.SectorDato.Size = New System.Drawing.Size(111, 20)
        Me.SectorDato.TabIndex = 40
        '
        'EdificioDato
        '
        Me.EdificioDato.Location = New System.Drawing.Point(92, 116)
        Me.EdificioDato.Margin = New System.Windows.Forms.Padding(2)
        Me.EdificioDato.Name = "EdificioDato"
        Me.EdificioDato.Size = New System.Drawing.Size(111, 20)
        Me.EdificioDato.TabIndex = 41
        '
        'PlantaDato
        '
        Me.PlantaDato.Location = New System.Drawing.Point(92, 156)
        Me.PlantaDato.Margin = New System.Windows.Forms.Padding(2)
        Me.PlantaDato.Name = "PlantaDato"
        Me.PlantaDato.Size = New System.Drawing.Size(111, 20)
        Me.PlantaDato.TabIndex = 42
        '
        'PuertaDato
        '
        Me.PuertaDato.Location = New System.Drawing.Point(92, 194)
        Me.PuertaDato.Margin = New System.Windows.Forms.Padding(2)
        Me.PuertaDato.Name = "PuertaDato"
        Me.PuertaDato.Size = New System.Drawing.Size(111, 20)
        Me.PuertaDato.TabIndex = 43
        '
        'Cerrar
        '
        Me.Cerrar.Location = New System.Drawing.Point(233, 189)
        Me.Cerrar.Margin = New System.Windows.Forms.Padding(2)
        Me.Cerrar.Name = "Cerrar"
        Me.Cerrar.Size = New System.Drawing.Size(76, 29)
        Me.Cerrar.TabIndex = 44
        Me.Cerrar.Text = "Cerrar"
        Me.Cerrar.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(233, 111)
        Me.Button3.Margin = New System.Windows.Forms.Padding(2)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(76, 29)
        Me.Button3.TabIndex = 45
        Me.Button3.Text = "Modificar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Volver
        '
        Me.Volver.Location = New System.Drawing.Point(233, 149)
        Me.Volver.Margin = New System.Windows.Forms.Padding(2)
        Me.Volver.Name = "Volver"
        Me.Volver.Size = New System.Drawing.Size(76, 29)
        Me.Volver.TabIndex = 46
        Me.Volver.Text = "Volver"
        Me.Volver.UseVisualStyleBackColor = True
        '
        'IdViviendaDato
        '
        Me.IdViviendaDato.Location = New System.Drawing.Point(29, 42)
        Me.IdViviendaDato.Margin = New System.Windows.Forms.Padding(2)
        Me.IdViviendaDato.Name = "IdViviendaDato"
        Me.IdViviendaDato.Size = New System.Drawing.Size(111, 20)
        Me.IdViviendaDato.TabIndex = 48
        '
        'Logo
        '
        Me.Logo.Image = Global.Civerpunck.My.Resources.Resources.logo
        Me.Logo.Location = New System.Drawing.Point(237, 12)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(80, 80)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Logo.TabIndex = 55
        Me.Logo.TabStop = False
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(329, 229)
        Me.Controls.Add(Me.Logo)
        Me.Controls.Add(Me.IdViviendaDato)
        Me.Controls.Add(Me.Volver)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Cerrar)
        Me.Controls.Add(Me.PuertaDato)
        Me.Controls.Add(Me.PlantaDato)
        Me.Controls.Add(Me.EdificioDato)
        Me.Controls.Add(Me.SectorDato)
        Me.Controls.Add(Me.PuertaLabel)
        Me.Controls.Add(Me.PlantaLabel)
        Me.Controls.Add(Me.EdificioLabel)
        Me.Controls.Add(Me.SectorLabel)
        Me.Controls.Add(Me.IdViviendaLabel)
        Me.Name = "Form3"
        Me.Text = "Form3"
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents IdViviendaLabel As Label
    Friend WithEvents SectorLabel As Label
    Friend WithEvents EdificioLabel As Label
    Friend WithEvents PlantaLabel As Label
    Friend WithEvents PuertaLabel As Label
    Friend WithEvents SectorDato As TextBox
    Friend WithEvents EdificioDato As TextBox
    Friend WithEvents PlantaDato As TextBox
    Friend WithEvents PuertaDato As TextBox
    Friend WithEvents Cerrar As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Volver As Button
    Friend WithEvents IdViviendaDato As TextBox
    Friend WithEvents Logo As PictureBox
End Class
