﻿Public Class Estadisticas
    Private capFisica As Int32
    Private capIntel As Int32
    Private empatia As Int32
    Private impetu As Int32
    Private espiritualidad As Int32

    Public Sub New(ByVal capFisica As Int32, ByVal capIntel As Int32, ByVal empatia As Int32, ByVal impetu As Int32, ByVal espiritualidad As Int32)
        Me.capFisica = capFisica
        Me.capIntel = capIntel
        Me.empatia = empatia
        Me.impetu = impetu
        Me.espiritualidad = espiritualidad
    End Sub

    Public Sub setCapFisica(ByVal capFisica As Int32)
        Me.capFisica = capFisica
    End Sub

    Public Function getCapFisica() As Int32
        Return Me.capFisica
    End Function

    Public Sub setCapIntel(ByVal capIntel As Int32)
        Me.capIntel = capIntel
    End Sub

    Public Function getCapIntel() As Int32
        Return Me.capIntel
    End Function

    Public Sub setEmpatia(ByVal empatia As Int32)
        Me.empatia = empatia
    End Sub

    Public Function getEmpatia() As Int32
        Return Me.empatia
    End Function

    Public Sub setImpetu(ByVal impetu As Int32)
        Me.impetu = impetu
    End Sub

    Public Function getImpetu() As Int32
        Return Me.impetu
    End Function

    Public Sub setEspiritualidad(ByVal espiritualidad As Int32)
        Me.espiritualidad = espiritualidad
    End Sub

    Public Function getEspiritualidad() As Int32
        Return Me.espiritualidad
    End Function

End Class
