﻿
Imports MySql.Data.MySqlClient
Public Class Form3

    Dim connexion As MySqlConnection
    Dim query As String
    Dim vivienda As New Vivienda("1", "1", "1", "1", "1")

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Form4.contador = 1 Then
            connectar()
            CargarVivienda()

        ElseIf Form4.contador = 2 Then
            connectar()
            CargarVivienda2()

        ElseIf Form4.contador = 3 Then
            connectar()
            CargarVivienda3()

        ElseIf Form4.contador = 4 Then
            connectar()
            CargarVivienda4()

        ElseIf Form4.contador = 5 Then
            connectar()
            CargarVivienda5()

        ElseIf Form4.contador = 6 Then
            connectar()
            CargarVivienda6()

        ElseIf Form4.contador = 7 Then
            connectar()
            CargarVivienda7()

        ElseIf Form4.contador = 8 Then
            connectar()
            CargarVivienda8()

        ElseIf Form4.contador = 9 Then
            connectar()
            CargarVivienda9()

        ElseIf Form4.contador = 10 Then
            connectar()
            CargarVivienda10()

        ElseIf Form4.contador = 11 Then
            connectar()
            CargarVivienda11()

        Else
            connectar()
            CargarVivienda12()

        End If
    End Sub

    Private Sub connectar()
        Try
            connexion = New MySqlConnection()
            connexion.ConnectionString = "server=localhost;user id=root;password=;database=vivienda"
            connexion.Open()
        Catch
            MessageBox.Show("Error")
        End Try
    End Sub

    Private Sub desconnectar()
        connexion.Close()
    End Sub

    Private Sub CargarVivienda()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(0).Item(0))
        vivienda.setSector(conjunt_dades.Rows(0).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(0).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(0).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(0).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda2()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(1).Item(0))
        vivienda.setSector(conjunt_dades.Rows(1).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(1).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(1).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(1).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda3()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(2).Item(0))
        vivienda.setSector(conjunt_dades.Rows(2).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(2).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(2).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(2).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda4()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(3).Item(0))
        vivienda.setSector(conjunt_dades.Rows(3).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(3).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(3).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(3).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda5()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(4).Item(0))
        vivienda.setSector(conjunt_dades.Rows(4).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(4).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(4).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(4).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda6()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(5).Item(0))
        vivienda.setSector(conjunt_dades.Rows(5).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(5).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(5).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(5).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda7()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(6).Item(0))
        vivienda.setSector(conjunt_dades.Rows(6).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(6).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(6).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(6).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda8()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(7).Item(0))
        vivienda.setSector(conjunt_dades.Rows(7).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(7).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(7).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(7).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda9()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(8).Item(0))
        vivienda.setSector(conjunt_dades.Rows(8).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(8).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(8).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(8).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda10()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(9).Item(0))
        vivienda.setSector(conjunt_dades.Rows(9).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(9).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(9).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(9).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda11()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(10).Item(0))
        vivienda.setSector(conjunt_dades.Rows(10).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(10).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(10).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(10).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub

    Private Sub CargarVivienda12()
        query = "SELECT * FROM vivienda"
        Dim comanda As New MySqlCommand(query, connexion)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)
        vivienda.setIdVivienda(conjunt_dades.Rows(11).Item(0))
        vivienda.setSector(conjunt_dades.Rows(11).Item(1))
        vivienda.setEdificio(conjunt_dades.Rows(11).Item(2))
        vivienda.setPlanta(conjunt_dades.Rows(11).Item(3))
        vivienda.setPuerta(conjunt_dades.Rows(11).Item(4))
        IdViviendaDato.Text = vivienda.getIdVivienda()
        SectorDato.Text = vivienda.getSector()
        EdificioDato.Text = vivienda.getEdificio()
        PlantaDato.Text = vivienda.getPlanta()
        PuertaDato.Text = vivienda.getPuerta()
    End Sub




    Private Sub Volver_Click(sender As Object, e As EventArgs) Handles Volver.Click
        Me.Hide()
    End Sub

    Private Sub Cerrar_Click(sender As Object, e As EventArgs) Handles Cerrar.Click
        Me.Close()
        desconnectar()
    End Sub


End Class