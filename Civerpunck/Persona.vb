﻿Public Class Persona

    Private id As Int32
    Private gremio As String
    Private nombre As String
    Private apellido As String
    Private genero As String
    Private sexo As String
    Private edad As Int32
    Private etnia As String
    Private estadoCivil As String
    Private dieta As String

    Public Sub New(ByVal id As Int32, ByVal gremio As String, ByVal nombre As String, ByVal apellido As String, ByVal genero As String, ByVal sexo As String, ByVal edad As Int32, ByVal etnia As String, ByVal estadoCivil As String, ByVal dieta As String)
        Me.id = id
        Me.gremio = gremio
        Me.nombre = nombre
        Me.apellido = apellido
        Me.genero = genero
        Me.sexo = sexo
        Me.edad = edad
        Me.etnia = etnia
        Me.estadoCivil = estadoCivil
        Me.dieta = dieta
    End Sub

    Public Sub setId(ByVal id As Int32)
        Me.id = id
    End Sub

    Public Function getId() As Int32
        Return Me.id
    End Function

    Public Sub setGremio(ByVal gremio As String)
        Me.gremio = gremio
    End Sub

    Public Function getGremio() As String
        Return Me.gremio
    End Function

    Public Sub setNombre(ByVal nombre As String)
        Me.nombre = nombre
    End Sub

    Public Function getNombre() As String
        Return Me.nombre
    End Function

    Public Sub setApellido(ByVal apellido As String)
        Me.apellido = apellido
    End Sub

    Public Function getApellido() As String
        Return Me.apellido
    End Function

    Public Sub setGenero(ByVal genero As String)
        Me.genero = genero
    End Sub

    Public Function getGenero() As String
        Return Me.genero
    End Function

    Public Sub setSexo(ByVal sexo As String)
        Me.sexo = sexo
    End Sub

    Public Function getSexo() As String
        Return Me.sexo
    End Function

    Public Sub setEdad(ByVal edad As String)
        Me.edad = edad
    End Sub

    Public Function getEdad() As String
        Return Me.edad
    End Function

    Public Sub setEtnia(ByVal etnia As String)
        Me.etnia = etnia
    End Sub

    Public Function getEtnia() As String
        Return Me.etnia
    End Function

    Public Sub setEstadoCivil(ByVal estadoCivil As String)
        Me.estadoCivil = estadoCivil
    End Sub

    Public Function getEstadoCivil() As String
        Return Me.estadoCivil
    End Function

    Public Sub setDieta(ByVal dieta As String)
        Me.dieta = dieta
    End Sub

    Public Function getDieta() As String
        Return Me.dieta
    End Function


End Class
