﻿Public Class Vivienda
    Private idVivienda As Int32
    Private sector As String
    Private edificio As String
    Private planta As String
    Private puerta As String

    Public Sub New(ByVal idVivienda As Int32, ByVal sector As String, ByVal edificio As String, ByVal planta As Int32, ByVal puerta As Int32)
        Me.idVivienda = idVivienda
        Me.sector = sector
        Me.edificio = edificio
        Me.planta = planta
        Me.puerta = puerta
    End Sub

    Public Sub setIdVivienda(ByVal idVivienda As Int32)
        Me.idVivienda = idVivienda
    End Sub

    Public Function getIdVivienda() As Int32
        Return Me.idVivienda
    End Function

    Public Sub setSector(ByVal sector As Int32)
        Me.sector = sector
    End Sub

    Public Function getSector() As Int32
        Return Me.sector
    End Function

    Public Sub setEdificio(ByVal edificio As Int32)
        Me.edificio = edificio
    End Sub

    Public Function getEdificio() As Int32
        Return Me.edificio
    End Function

    Public Sub setPlanta(ByVal planta As Int32)
        Me.planta = planta
    End Sub

    Public Function getPlanta() As Int32
        Return Me.planta
    End Function

    Public Sub setPuerta(ByVal puerta As Int32)
        Me.puerta = puerta
    End Sub

    Public Function getPuerta() As Int32
        Return Me.puerta
    End Function
End Class
